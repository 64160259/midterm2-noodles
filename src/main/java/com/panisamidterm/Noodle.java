package com.panisamidterm;
import java.util.Random;
public class Noodle {
    private String nood; //เส้น
    private String soup; //น้ำซุป
    private String meat; //เนื้อสัตว์
    private String veg; //ผัก
    
    public Noodle() {
        Random rand = new Random();
        setNood(rand.nextInt(6));
        setSoup(rand.nextInt(6));
        setMeat(rand.nextInt(2));
        setVeg(rand.nextInt(3));
    }

    public void setNood(int type) {
        String[] allType = {"Small Noodles", "Big flat Noodles", "Vermicelli", "Egg Noodles", "Glass Noodles", "Instant Noodles"};
        this.nood = allType[type];
    }

    public void setSoup(int type) {
        String[] allType = {"Clear Soup", "Thicken Soup", "Hot and Sour Soup", "Yentafo", "Suki", "Dry Noodle"};
        this.soup = allType[type];
    }

    public void setMeat(int type) {
        String[] allType = {"Pork","Beef"};
        this.meat = allType[type];
    }

    public void setVeg(int type) {
        String[] allType = {"Bean Sprout","Water Spinach","Cabbage"};
        this.veg = allType[type];
    } 
    public String toString() {
        return String.format("Nood : %s , Soup : %s , Meat : %s , Veg : %s",nood,soup,meat,veg);
    }
}
